var gulp = require('gulp')
var sass = require('gulp-sass')
var postcss = require('gulp-postcss')
var autoprefixer = require('autoprefixer')
var concat = require('gulp-concat')
var sourcemaps = require('gulp-sourcemaps')

/**
 * Set up all of our directory variables
 */
var dir = {
    sass: './src/sass/',
    js: './src/js/'
}

/**
 * Compiles scss and perform postcss transforms
 */
gulp.task('sass', function () {
    var processors = [
        autoprefixer
    ]
    return gulp.src(dir.sass + 'style.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
            .pipe(postcss(processors))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./'))
})

/**
 * Concat and uglify javascript
 */

 gulp.task('js', function() {
     return gulp.src(dir.js+'*.js')
     .pipe(sourcemaps.init())
         .pipe(concat('script.js'))
     .pipe(sourcemaps.write('./maps'))
     .pipe(gulp.dest('./'));
 });

/**
 * Monitors files for changes and triggers tasks
 */
gulp.task('watch', function () {
    gulp.watch(dir.sass+'**/*.scss', ['sass'])
})

gulp.task('default', ['watch'])
