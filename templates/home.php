<?php
/**
 * Template Name: Home
 * @package SiteSmash Foundation
 */

 get_header(); ?>

 	<div id="primary" class="content-area">
 		<main id="main" class="site-main" role="main">

            <!--Template Stuff Goes Here-->

 		</main><!-- #main -->
 	</div><!-- #primary -->

 <?php
 get_footer();
